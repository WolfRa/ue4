﻿// my_stack.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

template<typename T> class Stack
{
public:
	~Stack() {
		delete[] my_stack;
	}

	void pop() {
		T* temp = NULL;
		if (stack_size != 0) {
			stack_size--;
			temp = new T[stack_size];
			for (int i = 0; i < stack_size; i++) {
				temp[i] = my_stack[i];
			}

			delete[] my_stack;

			my_stack = temp;
		}
		else cout << "stack is empty!" << endl;
	}

	void push(T elm) {
		T* temp = NULL;
		stack_size++;
		temp = new T[stack_size];
		for (int i = 0; i < stack_size; i++) {
			if (i + 1 == stack_size) {
				temp[i] = elm;
			}
			else {
				temp[i] = my_stack[i];
			}
		}

		my_stack = temp;
	}

	void viewArray() {
		if (stack_size == 0) cout << "stack is empty!" << endl;
		else {
			cout << "stack elements: ";
			for (int i = 0; i < stack_size; i++) {
				cout << my_stack[i] << ' ';
			}
		}
	}

private:
	int stack_size = 0;
	T* my_stack = new T[stack_size];
};



int main()
{
	Stack<int> s;
	//s.pop();
	for (float i = 0.f; i < 10.f; i++) {
		s.push(i);
	}
	s.pop();
	s.viewArray();
}
