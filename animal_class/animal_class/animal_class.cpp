﻿// animal_class.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice() {
		cout << "It is animal" << endl;
	}
};

class Dog : public Animal
{
	void Voice() override {
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal
{
	void Voice() override {
		cout << "Meow!" << endl;
	}
};

class Cow : public Animal
{
	void Voice() override {
		cout << "Moo!" << endl;
	}
};

int main()
{
	Dog dog;
	Cat cat;
	Cow cow;
	Animal* animals[3] = { &dog, &cat, &cow };

	for (int i = 0; i < 3; i++) {
		animals[i]->Voice();
	}
}
