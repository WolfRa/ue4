﻿// print_string.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

int main()
{
	std::string my_string = "Hello world!";

	std::cout << my_string << std::endl;
	std::cout << my_string.length() << std::endl;
	std::cout << my_string[0] << std::endl;
	std::cout << my_string[my_string.length() - 1] << std::endl;
}

