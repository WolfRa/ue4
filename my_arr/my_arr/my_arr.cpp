﻿#include <iostream>
using namespace std;

int main()
{
	const int N = 3;
	int my_arr[N][N];
	int sum = 0;
	int indx = 0;

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			my_arr[i][j] = i + j;
			cout << my_arr[i][j] << " ";
		}
		cout << endl;

		if (16 % N == i) {
			indx = i;
			for (int k = 0; k < N; k++) {
				sum += my_arr[i][k];
			}
		}
	}

	cout << endl;

	cout << "Line index: " << indx << endl;
	cout << "Sum: " << sum << endl;
}

