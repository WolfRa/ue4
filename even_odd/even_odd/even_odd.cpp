﻿// even_odd.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

void even_odd(int N, bool is_odd) {
	for (int i = is_odd; i < N; i += 2) {
		std::cout << i << " ";
	}
}

int main()
{
	int N = 5;
	for (int i = 0; i < N; i += 2) {
		std::cout << i << " ";
	}
	std::cout << std::endl;

	even_odd(23, true);
}

