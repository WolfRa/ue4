﻿// my_class.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

class Vector
{
public:
	Vector() : x(5), y(5), z(5)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show() {
		cout << '\n' << x << ' ' << y << ' ' << z;
	}

	double GetLength() {
		return sqrt(x * x + y * y + z * z);
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

class my_class
{
public:
	my_class() : heigth(0), width(0)
	{}

	my_class(double _heigth, double _width) : heigth(_heigth), width(_width)
	{}

	void showVar() {
		cout << "heigth: " << heigth << ' ' << "width: " << width << endl;
	}
private:
	double heigth;
	double width;
};

int main()
{
	my_class cl;
	my_class cl1(10, 5);
	cl.showVar();
	cl1.showVar();
	Vector v(10, 5, 4);
	cout << "vector lenght: " << v.GetLength() << endl;
}

